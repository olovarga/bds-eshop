BDS Project 3 - E-shop database application with GUI


To build&run the project
Enter the following command in the project root directory to build the project.

$ mvn clean install


Run the project:

$ java -jar target/bds-eshop-1.0.0.jar


Sign-in with the following credentials:

Username: GottemS@gmail.com

Password: batman
