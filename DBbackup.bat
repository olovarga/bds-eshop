    @echo off
   for /f "tokens=1-4 delims=/ " %%i in ("%date%") do (
     set dow=%%i
     set month=%%j
     set day=%%k
     set year=%%l
   )
   set datestr=%month%_%day%_%year%
   echo datestr is %datestr%
    
   set BACKUP_FILE=BDS3backup_%datestr%.backup
   echo backup file name is %BACKUP_FILE%
   SET PGPASSWORD=postgres
   echo on
   bin\pg_dump -U postgres -F p postgres > C:\bds project 3 backup