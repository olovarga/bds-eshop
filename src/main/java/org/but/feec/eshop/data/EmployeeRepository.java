package org.but.feec.eshop.data;

import org.but.feec.eshop.api.*;
import org.but.feec.eshop.config.DataSourceConfig;
import org.but.feec.eshop.exceptions.DataAccessException;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import org.but.feec.eshop.exceptions.ExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;



public class EmployeeRepository {
    private static final Logger logger = LoggerFactory.getLogger(EmployeeRepository.class);

    public EmployeeAuthView findEmployeeByEmail(String email) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT mail, password" +
                             " FROM public.\"Employee\" e" +
                             " WHERE e.mail = ?")
        ) {
            preparedStatement.setString(1, email);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToEmployeeAuth(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Finding an employee failed.", e);
        }
        return null;
    }

    public EmployeeDetailView findEmployeeDetailView(Long employeeId) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT e.employee_id,first_name,last_name, mail, phone, r.role_id, role_type" +
                             " FROM public.\"Employee\" e" +
                             " LEFT JOIN public.\"Employee_role\" r ON e.employee_id = r.employee_id" +
                             " LEFT JOIN public.\"Role\" o ON r.role_id = o.role_id" +
                             " WHERE e.employee_id = ?; ")
        ) {
            preparedStatement.setLong(1, employeeId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToEmployeeDetailView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Finding an employee by ID with addresses failed.", e);
        }
        return null;
    }

    public List<EmployeeView> getEmployeeView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT employee_id, first_name, last_name, mail, phone" +
                             " FROM public.\"Employee\"");
             ResultSet resultSet = preparedStatement.executeQuery();) {
            List<EmployeeView> employeeViews = new ArrayList<>();
            while (resultSet.next()) {
                employeeViews.add(mapToEmployeeView(resultSet));
            }
            return employeeViews;
        } catch (SQLException e) {
            throw new DataAccessException("Employees' view could not be loaded.", e);
        }
    }

    public void createEmployee(EmployeeCreateView employeeCreateView){
        String insertEmployeeSQL = "INSERT INTO public.\"Employee\" (mail, first_name, last_name, password, phone) VALUES (?,?,?,?,?)";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertEmployeeSQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, employeeCreateView.getEmail());
            preparedStatement.setString(2, employeeCreateView.getFirstName());
            preparedStatement.setString(3, employeeCreateView.getLastName());
            preparedStatement.setString(4, String.valueOf(employeeCreateView.getPassword()));
            preparedStatement.setString(5, employeeCreateView.getPhone());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DataAccessException("Creating employee failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating employee failed, operation on the database failed.");
        }

    }

    public void editEmployee(EmployeeEditView employeeEditView){
        String insertEmployeeSQL = "UPDATE public.\"Employee\" e SET mail = ?, first_name = ?, last_name = ?, phone = ? WHERE e.employee_id = ?";
        String checkIfExists = "SELECT mail FROM public.\"Employee\" e WHERE e.employee_id = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertEmployeeSQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, employeeEditView.getEmail());
            preparedStatement.setString(2, employeeEditView.getFirstName());
            preparedStatement.setString(3, employeeEditView.getLastName());
            preparedStatement.setString(4, employeeEditView.getPhone());
            preparedStatement.setLong(5, employeeEditView.getId());

            try {
                connection.setAutoCommit(false);
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, employeeEditView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This person for edit do not exists.");
                }

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new DataAccessException("Creating employee failed, no rows affected.");
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating employee failed, operation on the database failed.");
        }

    }

    public boolean removeEmployee(int id) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement prpstmt = connection.prepareStatement(
                     "DELETE FROM public.\"Employee\" e " +
                             "WHERE e.employee_id = ?;"
             )) {
            prpstmt.setInt(1, id);
            prpstmt.executeUpdate();
            return true;
        }
        catch (SQLException e) {
            ExceptionHandler.handleException(e);

        }
        return false;
    }

    public List<SQLInjectionView> getSQLInjectionView (String input){
        String SQLquery = "SELECT title, release_year FROM public.\"first_test_table\" f WHERE f.title="+input;

        try (Connection connection = DataSourceConfig.getConnection();
             Statement stmnt = connection.createStatement();
             ResultSet resultSet = stmnt.executeQuery(SQLquery)){
            List<SQLInjectionView> injectionViews = new ArrayList<>();
            while(resultSet.next()){
                injectionViews.add(mapToSQLInjectionView(resultSet));
            }
            return injectionViews;
        }catch (SQLException e){
            throw new DataAccessException("The SQLInjection operation has failed.",e);
        }
    }

    private EmployeeAuthView mapToEmployeeAuth(ResultSet rs) throws SQLException {
        EmployeeAuthView employee = new EmployeeAuthView();
        employee.setEmail(rs.getString("mail"));
        employee.setPassword(rs.getString("password"));
        return employee;
    }



    private EmployeeDetailView mapToEmployeeDetailView(ResultSet rs) throws SQLException{
        EmployeeDetailView employeeDetailView = new EmployeeDetailView();
        employeeDetailView.setId(rs.getLong("employee_id"));
        employeeDetailView.setFirstName(rs.getString("first_name"));
        employeeDetailView.setLastName(rs.getString("last_name"));
        employeeDetailView.setMail(rs.getString("mail"));
        employeeDetailView.setPhone(rs.getString("phone"));
        employeeDetailView.setRoleId(rs.getLong("role_id"));
        employeeDetailView.setRoleType(rs.getString("role_type"));
        return employeeDetailView;
    }



    private EmployeeView mapToEmployeeView(ResultSet rs) throws SQLException {
            EmployeeView employeeView = new EmployeeView();
            employeeView.setId(rs.getLong("employee_id"));
            employeeView.setFirstName(rs.getString("first_name"));
            employeeView.setLastName(rs.getString("last_name"));
            employeeView.setMail(rs.getString("mail"));
            employeeView.setPhone(rs.getString("phone"));
            return employeeView;
        }

    private SQLInjectionView mapToSQLInjectionView(ResultSet rs) throws SQLException{
        SQLInjectionView sqlInjectionView = new SQLInjectionView();
        sqlInjectionView.setTitle(rs.getString("title"));
        sqlInjectionView.setYear(rs.getLong("release_year"));

        return sqlInjectionView;
    }
}






