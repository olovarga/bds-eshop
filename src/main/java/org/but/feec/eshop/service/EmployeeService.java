package org.but.feec.eshop.service;

import org.but.feec.eshop.api.*;
import org.but.feec.eshop.data.EmployeeRepository;
import at.favre.lib.crypto.bcrypt.BCrypt;

import java.util.List;

public class EmployeeService {
    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {

        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeView> getEmployeeView(){
        return employeeRepository.getEmployeeView();
    }

    public EmployeeDetailView getEmployeeDetailView(Long id){
        return employeeRepository.findEmployeeDetailView(id);
    }

    public void createEmployee(EmployeeCreateView employeeCreateView){
        char[] originalPassword = employeeCreateView.getPassword();
        char[] hashedPassword = hashPassword(originalPassword);
        employeeCreateView.setPassword(hashedPassword);

        employeeRepository.createEmployee(employeeCreateView);
    }

    public void editEmployee(EmployeeEditView employeeEditView){
        employeeRepository.editEmployee(employeeEditView);
    }

    public List<SQLInjectionView> getSQLInjectionView(String input){
        return employeeRepository.getSQLInjectionView(input);
    }

 /**   public List<EmployeeSearchView> getEmployeeSearchView(String text){
        return employeeRepository.searchEmployee(text);
    }
  */





    /**
     * <p>
     * Note: For implementation details see: https://github.com/patrickfav/bcrypt
     * </p>
     *
     * @param password to be hashed
     * @return hashed password
     */

    private char[] hashPassword(char[] password){
        return BCrypt.withDefaults().hashToChar(12,password);
    }



}
