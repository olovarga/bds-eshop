package org.but.feec.eshop.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.eshop.api.SQLInjectionView;
import org.but.feec.eshop.data.EmployeeRepository;
import org.but.feec.eshop.service.EmployeeService;

import java.util.List;

public class SQLInjectionController {

    @FXML
    private TextField sqlField;


    @FXML
    private TableColumn<SQLInjectionView, String> title;

    @FXML
    private TableColumn<SQLInjectionView, Long> year;

    @FXML
    private TableView<SQLInjectionView> employeesTableView;

    private EmployeeService employeeService;
    private EmployeeRepository employeeRepository;

    public Stage stage;
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void initialize() {
        employeeRepository = new EmployeeRepository();
        employeeService = new EmployeeService(employeeRepository);


        title.setCellValueFactory(new PropertyValueFactory<SQLInjectionView, String>("title"));
        year.setCellValueFactory(new PropertyValueFactory<SQLInjectionView, Long>("year"));

    }

    private ObservableList<SQLInjectionView> initializeEmployeesData() {

        String input = sqlField.getText();
        List<SQLInjectionView> employees = employeeService.getSQLInjectionView(input);
        return FXCollections.observableArrayList(employees);
    }


    public void handleInjectionButton(ActionEvent actionEvent){
        ObservableList<SQLInjectionView> observableEmployeeList = initializeEmployeesData();
        employeesTableView.setItems(observableEmployeeList);

    }





}
