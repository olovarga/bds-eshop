package org.but.feec.eshop.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.eshop.App;
import org.but.feec.eshop.api.EmployeeView;
import org.but.feec.eshop.api.EmployeeDetailView;
import org.but.feec.eshop.data.EmployeeRepository;
import org.but.feec.eshop.exceptions.ExceptionHandler;
import org.but.feec.eshop.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Optional;


public class EmployeesController {
    private static final Logger logger = LoggerFactory.getLogger(EmployeesController.class);


    @FXML
    private TableView<EmployeeView> EmployeeView;

    @FXML
    private Button addEmployeeButton;

    @FXML
    private Button refreshButton;

    @FXML
    private Button searchButton;

    @FXML
    private TextField employeeSearchField;

    @FXML
    private TableColumn<EmployeeView, String> employeesEmail;

    @FXML
    private TableColumn<EmployeeView, String> employeesFirstName;

    @FXML
    private TableColumn<EmployeeView, Long> employeesId;

    @FXML
    private TableColumn<EmployeeView, String> employeesLastName;

    @FXML
    private TableColumn<EmployeeView, String> employeesPhone;






    private EmployeeService employeeService;
    private EmployeeRepository employeeRepository;

    public EmployeesController() {
    }

    @FXML
    private void initialize() {
        employeeRepository = new EmployeeRepository();
        employeeService = new EmployeeService(employeeRepository);

        employeesId.setCellValueFactory(new PropertyValueFactory<EmployeeView, Long>("id"));
        employeesPhone.setCellValueFactory(new PropertyValueFactory<EmployeeView, String>("phone"));
        employeesEmail.setCellValueFactory(new PropertyValueFactory<EmployeeView, String>("mail"));
        employeesFirstName.setCellValueFactory(new PropertyValueFactory<EmployeeView, String>("firstName"));
        employeesLastName.setCellValueFactory(new PropertyValueFactory<EmployeeView, String>("lastName"));

        ObservableList<EmployeeView> observableEmployeeList = initializeEmployeesData();
        EmployeeView.setItems(observableEmployeeList);

        EmployeeView.getSortOrder().add(employeesId);
        initializeTableViewSelection();


        logger.info("EmployeeController initialized");
    }

        private void initializeTableViewSelection () {
            MenuItem edit = new MenuItem("Edit employee");
            MenuItem detailedView = new MenuItem("Detailed employee view");
            MenuItem delete = new MenuItem("Delete employee");
            edit.setOnAction((ActionEvent event) -> {
                EmployeeView employeeView = EmployeeView.getSelectionModel().getSelectedItem();
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(App.class.getResource("EmployeeEdit.fxml"));
                    Stage stage = new Stage();
                    stage.setUserData(employeeView);
                    stage.setTitle("BDS JavaFX Edit Employee");

                    EmployeeEditController controller = new EmployeeEditController();
                    controller.setStage(stage);
                    fxmlLoader.setController(controller);

                    Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                    stage.setScene(scene);

                    stage.show();
                } catch (IOException ex) {
                    ExceptionHandler.handleException(ex);
                }
            });

            detailedView.setOnAction((ActionEvent event) -> {
                EmployeeView employeeView = EmployeeView.getSelectionModel().getSelectedItem();
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(App.class.getResource("EmployeeDetailView.fxml"));
                    Stage stage = new Stage();

                    Long employeeId = employeeView.getId();
                    EmployeeDetailView employeeDetailView = employeeService.getEmployeeDetailView(employeeId);

                    stage.setUserData(employeeDetailView);
                    stage.setTitle("BDS JavaFX Employees Detailed View");

                    EmployeeDetailViewController controller = new EmployeeDetailViewController();
                    controller.setStage(stage);
                    fxmlLoader.setController(controller);

                    Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                    stage.setScene(scene);

                    stage.show();
                } catch (IOException ex) {
                    ExceptionHandler.handleException(ex);
                }
            });

            delete.setOnAction((ActionEvent event) -> {
                EmployeeView employeeView = EmployeeView.getSelectionModel().getSelectedItem();
                try {

                    Long employeeId = employeeView.getId();
                    employeeRepository.removeEmployee(Math.toIntExact(employeeId));

                    employeeDeletedConfirmation();

                } catch (Exception ex) {
                    ExceptionHandler.handleException(ex);
                }
            });

            ContextMenu menu = new ContextMenu();
            menu.getItems().add(edit);
            menu.getItems().addAll(detailedView);
            menu.getItems().add(delete);
            EmployeeView.setContextMenu(menu);





        }

        private ObservableList<EmployeeView> initializeEmployeesData(){
        List<EmployeeView> employees = employeeService.getEmployeeView();
            return FXCollections.observableArrayList(employees);
        }

    public void handleAddEmployeeButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("EmployeeCreate.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);
            Stage stage = new Stage();
            stage.setTitle("BDS JavaFX Create Employee");
            stage.setScene(scene);

//            Stage stageOld = (Stage) signInButton.getScene().getWindow();
//            stageOld.close();
//
//            stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/vut.jpg")));
//            authConfirmDialog();

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }


            private void employeeDeletedConfirmation() {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Employee deleted Confirmation");
                alert.setHeaderText("Your Employee was successfully deleted.");

                Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        alert.setResult(ButtonType.CANCEL);
                        alert.hide();
                    }
                }));
                idlestage.setCycleCount(1);
                idlestage.play();
                Optional<ButtonType> result = alert.showAndWait();
            }



    public void handleRefreshButton(ActionEvent actionEvent) {
        ObservableList<EmployeeView> observableEmployeeList = initializeEmployeesData();
        EmployeeView.setItems(observableEmployeeList);
        EmployeeView.refresh();
        EmployeeView.sort();
    }

    public void handleInjectionButton(ActionEvent actionEvent){
        try{

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("SQLInjection.fxml"));
            Stage stage = new Stage();
            SQLInjectionController injectionController = new SQLInjectionController();

            injectionController.setStage(stage);
            fxmlLoader.setController(injectionController);
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);


            stage.setTitle("SQL Injection on a dummy table.");
            stage.setScene(scene);
            stage.show();
        }catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }


    }

}
