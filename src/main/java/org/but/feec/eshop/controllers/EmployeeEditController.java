package org.but.feec.eshop.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.eshop.api.EmployeeView;
import org.but.feec.eshop.api.EmployeeEditView;
import org.but.feec.eshop.data.EmployeeRepository;
import org.but.feec.eshop.service.EmployeeService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class EmployeeEditController {
    private static final Logger logger = LoggerFactory.getLogger(EmployeeEditController.class);

    @FXML
    private Button editEmployeeButton;

    @FXML
    private TextField emailTextField;

    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField idTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    private TextField phoneTextField;

    private EmployeeService employeeService;
    private EmployeeRepository employeeRepository;
    private ValidationSupport validation;

    public Stage stage;

    public void setStage(Stage stage) {

        this.stage = stage;
    }

    @FXML
    public void initialize() {
        employeeRepository = new EmployeeRepository();
        employeeService = new EmployeeService(employeeRepository);

        validation = new ValidationSupport();
        validation.registerValidator(idTextField, Validator.createEmptyValidator("The id must not be empty."));
        idTextField.setEditable(false);
        validation.registerValidator(emailTextField, Validator.createEmptyValidator("The email must not be empty."));
        validation.registerValidator(firstNameTextField, Validator.createEmptyValidator("The first name must not be empty."));
        validation.registerValidator(lastNameTextField, Validator.createEmptyValidator("The last name must not be empty."));
        validation.registerValidator(phoneTextField, Validator.createEmptyValidator("The phone must not be empty."));

        editEmployeeButton.disableProperty().bind(validation.invalidProperty());

        loadEmployeesData();

        logger.info("EmployeeEditController initialized");
    }

    private void loadEmployeesData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof EmployeeView) {
            EmployeeView employeeView = (EmployeeView) stage.getUserData();
            idTextField.setText(String.valueOf(employeeView.getId()));
            emailTextField.setText(employeeView.getMail());
            firstNameTextField.setText(employeeView.getFirstName());
            lastNameTextField.setText(employeeView.getLastName());
            phoneTextField.setText(employeeView.getPhone());
        }
    }

    @FXML
    public void handleEditEmployeeButton(ActionEvent event) {
        Long id = Long.valueOf(idTextField.getText());
        String email = emailTextField.getText();
        String firstName = firstNameTextField.getText();
        String lastName = lastNameTextField.getText();
        String phone = phoneTextField.getText();

        EmployeeEditView employeeEditView = new EmployeeEditView();
        employeeEditView.setId(id);
        employeeEditView.setEmail(email);
        employeeEditView.setFirstName(firstName);
        employeeEditView.setLastName(lastName);
        employeeEditView.setPhone(phone);

        employeeService.editEmployee(employeeEditView);

        employeeEditedConfirmationDialog();
    }

    private void employeeEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Employee Edit Confirmation");
        alert.setHeaderText("Your employee was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }
}
