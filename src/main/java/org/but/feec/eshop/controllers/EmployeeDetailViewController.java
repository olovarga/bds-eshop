package org.but.feec.eshop.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.but.feec.eshop.api.EmployeeDetailView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmployeeDetailViewController {
    private static final Logger logger = LoggerFactory.getLogger(EmployeeDetailViewController.class);

    @FXML
    private TextField idTextField;

    @FXML
    private TextField emailTextField;

    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    private TextField phoneTextField;

    @FXML
    private TextField roleIdTextField;

    @FXML
    private TextField roleTypeTextField;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        idTextField.setEditable(false);
        emailTextField.setEditable(false);
        firstNameTextField.setEditable(false);
        lastNameTextField.setEditable(false);
        phoneTextField.setEditable(false);
        roleIdTextField.setEditable(false);
        roleTypeTextField.setEditable(false);

        loadEmployeesData();

        logger.info("EmployeeDetailViewController initialized");
    }

    private void loadEmployeesData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof EmployeeDetailView) {
            EmployeeDetailView employeeView = (EmployeeDetailView) stage.getUserData();
            idTextField.setText(String.valueOf(employeeView.getId()));
            emailTextField.setText(employeeView.getMail());
            firstNameTextField.setText(employeeView.getFirstName());
            lastNameTextField.setText(employeeView.getLastName());
            phoneTextField.setText(employeeView.getPhone());
            roleIdTextField.setText(String.valueOf(employeeView.getRoleId()));
            roleTypeTextField.setText(employeeView.getRoleType());
        }
    }


}
