package org.but.feec.eshop.api;

import javafx.beans.property.StringProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleLongProperty;


public class SQLInjectionView {
    private final StringProperty title = new SimpleStringProperty();
    private final LongProperty year = new SimpleLongProperty();

    public void setTitle(String title)
    {
        this.title.set(title);
    }

    public String getTitle(){
        return titleProperty().get();
    }

    public void setYear(Long year){
        this.year.set(year);
    }

    public Long getYear(){
        return yearProperty().get();
    }

    public StringProperty titleProperty(){
        return title;
    }

    public LongProperty yearProperty(){
        return year;
    }
}
