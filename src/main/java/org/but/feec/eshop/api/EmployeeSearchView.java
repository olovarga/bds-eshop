/**
package org.but.feec.eshop.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.List;

public class EmployeeSearchView {

    private final LongProperty id = new SimpleLongProperty();
    private final StringProperty firstName = new SimpleStringProperty();
    private final StringProperty lastName = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final StringProperty phone = new SimpleStringProperty();

    public void setId(long id) {

        this.id.set(id);
    }

    public void setFirstName(String firstName) {

        this.firstName.set(firstName);
    }

    public void setLastName(String lastName) {

        this.lastName.set(lastName);
    }

    public void setEmail(String email) {

        this.email.set(email);
    }

    public void setPhone(String phone) {

        this.phone.set(phone);
    }

    public long getId() {
        return idProperty().get();
    }


    public String getFirstName()
    {
        return employeeFirstNameProperty().get();
    }

    public String getLastName()
    {
        return employeeLastNameProperty().get();
    }

    public String getEmail()
    {
        return employeeEmailProperty().get();
    }

    public String getPhone() {

        return employeePhoneProperty().get();
    }

    public LongProperty idProperty() {

        return id;
    }

    public StringProperty employeeFirstNameProperty() {

        return firstName;
    }

    public StringProperty employeeLastNameProperty()
    {
        return lastName;
    }

    public StringProperty employeeEmailProperty()
    {
        return email;
    }

    public StringProperty employeePhoneProperty()
    {
        return phone;
    }








}
*/