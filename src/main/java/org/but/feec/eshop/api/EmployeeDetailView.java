package org.but.feec.eshop.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class EmployeeDetailView {

    private LongProperty id = new SimpleLongProperty();
    private StringProperty firstName = new SimpleStringProperty();
    private StringProperty lastName = new SimpleStringProperty();
    private StringProperty mail = new SimpleStringProperty();
    private StringProperty phone = new SimpleStringProperty();
    private LongProperty roleId = new SimpleLongProperty();
    private StringProperty roleType = new SimpleStringProperty();


    public Long getId() {
        return idProperty().get();
    }

    public void setId(Long id) {
        this.idProperty().setValue(id);
    }

    public String getMail() {
        return mailProperty().get();
    }

    public void setMail(String mail) {
        this.mailProperty().setValue(mail);
    }

    public String getFirstName() {
        return firstNameProperty().get();
    }

    public void setFirstName(String firstName) {
        this.firstNameProperty().setValue(firstName);
    }

    public String getLastName() {
        return lastNameProperty().get();
    }

    public void setLastName(String lastName) {
        this.lastNameProperty().setValue(lastName);
    }

    public Long getRoleId() {
        return roleIdProperty().get();
    }

    public void setRoleId(Long roleId) {
        this.roleIdProperty().setValue(roleId);
    }

    public String getPhone() {
        return phoneProperty().get();
    }

    public void setPhone(String phone) {
        this.phoneProperty().setValue(phone);
    }

    public String getRoleType() {
        return roleTypeProperty().get();
    }

    public void setRoleType(String roleType) {
        this.roleTypeProperty().setValue(roleType);
    }

    public LongProperty idProperty() {
        return id;
    }

    public StringProperty mailProperty() {
        return mail;
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public LongProperty roleIdProperty() {
        return roleId;
    }

    public StringProperty phoneProperty() {
        return phone;
    }

    public StringProperty roleTypeProperty() {
        return roleType;
    }





}
