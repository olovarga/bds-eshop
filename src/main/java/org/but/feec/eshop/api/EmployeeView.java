package org.but.feec.eshop.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class EmployeeView {

        private LongProperty id = new SimpleLongProperty();
        private StringProperty mail = new SimpleStringProperty();
        private StringProperty firstName = new SimpleStringProperty();
        private StringProperty lastName = new SimpleStringProperty();
        private StringProperty phone = new SimpleStringProperty();

        public Long getId() {

            return idProperty().get();
        }

        public void setId(Long id) {

            this.idProperty().setValue(id);
        }

        public String getMail() {
            return mailProperty().get();
        }

        public void setMail(String mail) {
            this.mailProperty().setValue(mail);
        }

        public String getFirstName() {
            return firstNameProperty().get();
        }

        public void setFirstName(String firstName) {
            this.firstNameProperty().setValue(firstName);
        }

        public String getLastName() {
            return lastNameProperty().get();
        }

        public void setLastName(String lastName) {
            this.lastNameProperty().setValue(lastName);
        }

        public String getPhone() {
            return phoneProperty().get();
        }

        public void setPhone(String phone) {
            this.phoneProperty().set(phone);
        }

        public LongProperty idProperty() {
            return id;
        }

        public StringProperty mailProperty() {
            return mail;
        }

        public StringProperty firstNameProperty() {
            return firstName;
        }

        public StringProperty lastNameProperty() {
            return lastName;
        }

        public StringProperty phoneProperty() {
            return phone;
        }

    }

