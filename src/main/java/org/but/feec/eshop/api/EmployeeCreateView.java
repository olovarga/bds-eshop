package org.but.feec.eshop.api;

import java.util.Arrays;

public class EmployeeCreateView {
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private char[] password;

    public String getFirstName(){
        return firstName;
    }

    public void setFirstName(String firstName){
        this.firstName=firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public void setLastName(String lastName){
        this.lastName=lastName;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email=email;
    }

    public String getPhone(){
        return phone;
    }

    public void setPhone(String phone){
        this.phone=phone;
    }

    public char[] getPassword(){
        return password;
    }

    public void setPassword(char[] password){
        this.password=password;
    }

    @Override
    public String toString(){
        return "EmployeeCreateView{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", password=" + Arrays.toString(password) +
                '}';

    }

}
